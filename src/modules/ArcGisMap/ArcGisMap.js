import React from 'react';
import { loadModules } from 'esri-loader';
import EsriLoaderReact from 'esri-loader-react';
import { Map } from '@esri/react-arcgis';
import { WebMap } from '@esri/react-arcgis';
import { MapView } from '@esri/react-arcgis';
import { esriConfig } from '@esri/react-arcgis';

const styles =  {
  container: {
    minHeight: '95vh',
    height: '95vh',
    width: '100%',
    margin: 0
  },
  mapDiv: {
    padding: 0,
    margin: 0,
    height: '100%',
    width: '100%'
  },
}

class ArcGisMap extends React.Component {
  constructor(props) {
    super(props);
    this.state = { fetching: false };
    loadModules(['esri/views/MapView', 'esri/WebMap','esri/config'])
  .then(([MapView, WebMap,esriConfig]) => {
    // then we load a web map from an id
    esriConfig.portalUrl = "https://stg-gis.flysfo.com/portal";
    var webmap = new WebMap({
      portalItem: { // autocasts as new PortalItem()
        id: '1ca86f635daa42ee873d77fecc085b62'
      }
    });
    // and we show that map in a container w/ id #viewDiv
    var view = new MapView({
      map: webmap,
      container: 'viewDiv'
    });
  }) 
  }
  render() {
    const options = {
      url: 'https://js.arcgis.com/4.6/'
    };
    return   <div style={styles.container}>
    <div id='viewDiv' style={ styles.mapDiv }></div>
</div>;
  }
}
export default ArcGisMap;