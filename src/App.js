import React, { Component } from 'react';
import './App.css';
import ArcGisMap from './modules/ArcGisMap/ArcGisMap';

class App extends Component {
  render() {
    return (
      <div className="App">
        <ArcGisMap />
      </div>
    );
  }
}

export default App;


